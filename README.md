# vRA blueprints

## Intro

Those blueprints are for homelab, demo, or any other simple environement.  
There is no support for those, but feel free to send issues/comment :)  

## Infrastructure

The infra validated/tested and needed is:

- vRA 8.1
- vSphere (vCenter + ESXi) 7.0
- NSX-T 3.0
- Ansible 2.7
- Ubuntu 18.04

## Configuration in infrastructure

- vCenter
  - configure normally - a datacenter, a cluster, some esxi hosts...
  - add the ubuntu template
- NSX-T
  - configure normally - transport zones, infra manager, uplink profiles, node profiles, edge nodes, tier0, tier1...
  - create one logical switch for the on-demand (ex name: ls-outbound)
  - create the logical switches for each project with the exact project name
  - create the security groups for each project with the exact project name, suffixed by _web, _app and _db

## Configuration in vRA

create the Cloud Accounts:

- vcsa
  - connected to the vcenter server
  - vcsa.corp.local
- nsx
  - connected to the NSX-T manager's vip
  - nsx.corp.local

create the Integrations:

- ansible
  - connected to the ansible master vm
  - install ansible with that guide: [Guide](https://www.techrepublic.com/article/how-to-install-ansible-on-ubuntu-server-18-04/)
  - prepare the master vm for vra following the guide: [Documentation](https://docs.vmware.com/en/vRealize-Automation/8.1/Using-and-Managing-Cloud-Assembly/GUID-9244FFDE-2039-48F6-9CB1-93508FCAFA75.html?hWord=N4IghgNiBc4HYGcCWAjCBTEBfIA)
  - in my case, login/pass of this vm is ubuntu/ubuntu
- gitlab
  - connected to the public gitlab, this repo configured for project-1
- vrops
  - connected to the vrops for costing and monitoring

## Resource Networks

Edit the discovered networks:

- VM Network (or the management network name)
  - set the cidr, gateway, dns...
  - add tags
    - net:management
    - cloud:onprem
  - create ip range attached to that network with a range of at least 5 ip
- ls-outbound (or the network for on-demand)
  - set the cidr, gateway, dns...

## Resource Compute

- Tag the Resource Compute with
  - cloud:onprem
  - env:prod
  - cluster:workloads
  - site:lab

## Resource Kubernetes

TO DO TO DO

## Network Profiles

TO DO TO DO

## Storage Profiles

TO DO TO DO

## Image and Flavor Mappings

TO DO TO DO

## Cloud Zones

TO DO TO DO

## Projects

TO DO TO DO

PACIFIC ?!



note on the environment:

- Flavors: `small`, `medium`
- Images: `ubuntu`

Those must exists to apply blueprints.
